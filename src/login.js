import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from './Utils/Common';
import {  useToasts } from 'react-toast-notifications';


function Login(props) {
  const [loading, setLoading] = useState(false);
  const username = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const { addToast } = useToasts();

  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios.post('http://localhost:5000/login', { email: username.value, userpwd: password.value }).then(response => {
      setLoading(false);
      setUserSession(response.data.Auth, response.data.user_id, response.data.name, response.data.user_type);
      addToast("Login Sucessfull", { appearance: 'success',autoDismiss: true });
      props.history.push('/dashboard');
    }).catch(error => {
      setLoading(false);
      addToast(error.response.data.message, { appearance: 'error',autoDismiss: true });
      if (error.response.status === 401) setError(error.response.data.message);
      else setError("Something went wrong. Please try again later.");
    });
  }

  return (
    <div className="container">
      <div id="login-row" class="row justify-content-center align-items-center">
        <div id="login-column" className="col-md-6">
          <div id="login-box" className="col-md-12"></div>
          <h3 className="text-center text-info"> Login</h3><br /><br />
          <div className="form-group">
            <label className="text-info">Username:</label>
            <input className="form-control" type="text" {...username} autoComplete="new-password" />
          </div>
          <div className="form-group" style={{ marginTop: 10 }}>
          <label className="text-info">Password:</label>
            <input className="form-control" type="password" {...password} autoComplete="new-password" />
          </div>
          {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
          <input type="button" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} /><br />
        </div>
      </div>
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default Login;