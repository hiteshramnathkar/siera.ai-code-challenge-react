import React, { useState, useEffect } from 'react';
import { BrowserRouter, Switch, Route, NavLink } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import { getUser, getUserid, getUsertype, removeUserSession } from './Utils/Common';
import ListEmployees from '../components/listemployees';
import AddEmployee from '../components/addemployee';
import ViewEmployee from '../components/viewemployee';
import AddPerformance from '../components/addperformancereview';
import ListPerformance from '../components/listreviews';
import Assigntopeer from '../components/assigntopeer';
import ListReviewPeer from '../components/listreviewspeer';
import Addfeedback from '../components/addfeedback';
import PrivateRoute from '../src/Utils/PrivateRoutes';


function Dashboard(props) {
  const user = getUser();
  const userid = getUserid();
  const usertype = getUsertype();

  // handle click event of logout button
  const handleLogout = () => {
    removeUserSession();
    props.history.push('/login');
  }

  var navigation;
  if (usertype==0) {
    navigation = <Nav className="mr-auto"> <NavLink className="nav-link" exact  to="/listemployees">List Employees  </NavLink> <NavLink className="nav-link"   to="/addemployee">Add Employees  </NavLink> <NavLink className="nav-link"   to="/listreviews">List Reviews  </NavLink> </Nav>;         
  }
  else if(usertype ==1)
  {
    navigation = <Nav className="mr-auto"> <NavLink className="nav-link" exact  to="/listreviewpeer">List Reviews for Me  </NavLink> </Nav>;   
  }

  return (
    <div>
      <BrowserRouter>

        Welcome {user}!<br />
        
        
        <Navbar bg="light" variant="light">
        {navigation}
        <input type="button" onClick={handleLogout} value="Logout" />
        </Navbar>
        
        <div className="content">
            <Switch>
              
              <PrivateRoute path="/listemployees" component={ListEmployees} />
              <PrivateRoute path="/addemployee" component={AddEmployee} />
              <PrivateRoute path="/edit/:id" component={ViewEmployee} />
              <PrivateRoute path="/addperformance/:id" component={AddPerformance} />
              <PrivateRoute path="/listreviews" component={ListPerformance} />
              <PrivateRoute path="/assigntopeer/:Id/:Period" component={Assigntopeer} />
              <PrivateRoute path="/listreviewpeer" component={ListReviewPeer} />
              <PrivateRoute path="/addfeedback/:Id/:Period" component={Addfeedback} />


            </Switch>
            
        </div>
        

      </BrowserRouter>

    </div>
  );
}

export default Dashboard;