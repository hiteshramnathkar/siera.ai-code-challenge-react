// return the user data from the session storage
export const getUser = () => {
    const userStr = sessionStorage.getItem('name');
    if (userStr) return JSON.parse(userStr);
    else return null;
  }
  
  // return the token from the session storage
  export const getToken = () => {
    return sessionStorage.getItem('token') || null;
  }
  
// return the token from the session storage
export const getUserid = () => {
  return sessionStorage.getItem('user') || null;
}

export const getUsertype = () => {
  const typeStr = sessionStorage.getItem('usertype');
    if (typeStr) return JSON.parse(typeStr);
    else return null;
}
  // remove the token and user from the session storage
  export const removeUserSession = () => {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('name');
    sessionStorage.removeItem('usertype');
  }
  
  // set the token and user from the session storage
  export const setUserSession = (token, user_id, name, user_type) => {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('user', JSON.stringify(user_id));
    sessionStorage.setItem('name', JSON.stringify(name));
    sessionStorage.setItem('usertype', JSON.stringify(user_type));
  }