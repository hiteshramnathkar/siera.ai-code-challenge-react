import React, { useState, Component } from 'react';
import axios from 'axios';
import { useToasts } from 'react-toast-notifications';
import { getToken, removeUserSession } from '../src/Utils/Common';
import { Link } from 'react-router-dom';


class listemployees extends Component {
    state = { data: [] }
    
    //Using ComponentDidMount to fetch data from api & Set State to populate the list
    componentDidMount() {
        const token = getToken();
        axios.get('http://localhost:5000/people', {
            headers: {
                Authorization: "Bearer " + token
            }
        })
            .then(res => {
                const data = res.data;
                this.setState({ data });

            })
            .catch(error => {
            
                alert(error.response.data.message);
                
            })
    }

    //function to delete an employee, the parameter passed is the employees id
    
    deleteRow(id, e) {
        const token = getToken();
        
    //Used Axios delete request to Delete employees while passing the token received on Login with the employees id

        axios.delete(`http://localhost:5000/people/${id}`, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
            .then(res => {
                console.log(res);
                console.log(this.state.data);
                const data = this.state.data.filter(item => item.id !== id);
                console.log(data);
                alert("Employee Deleted sucessfully");
                
                this.setState({ data });
            })
            .catch(error => {
                alert(error.response.data.message);
            })

    }

    render() {

        return (
            <div>
                <Header />


                <table  className="table">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Designation</th>
                            <th>Department</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((person) => (
                            <tr>
                                <td>
                                    {person.id}
                                </td>
                                <td>
                                    {person.fname}
                                </td>
                                <td>
                                    {person.email}
                                </td>
                                <td>
                                    {person.department}
                                </td>
                                <td>
                                    {person.designation}
                                </td>

                                <td>
                                    <Link to={"/addperformance/" + person.id}>Add Review</Link> | <Link to={"/edit/" + person.id}>Edit</Link>  | <button className="btn btn-danger" onClick={(e) => this.deleteRow(person.id, e)}>Delete</button>

                                </td>

                            </tr>
                        )
                        )}

                    </tbody>
                </table>
            </div >
        );
    }
}

class Header extends Component {
    render() {
        return (
            <div>
                <h3>Employee List</h3>
            </div>

        );
    }
}


export default listemployees;