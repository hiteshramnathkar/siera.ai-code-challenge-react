import React, { Component } from 'react';
import axios from 'axios';
import {  useToasts } from 'react-toast-notifications';
import { getToken } from '../src/Utils/Common';

export default class assigntopeer extends Component {

    state = { data: [] }
    
    //Using ComponentDidMount to fetch data from api & Set State to populate the list

    componentDidMount() {
        const token = getToken();
        axios.get('http://localhost:5000/people', {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then(res => {
                const data = res.data;
                this.setState({ data });
                console.log(this.state.data);
            })
        .catch(error => {
            alert(error.response.data.message);
              });
    }

    //function to assign review to a peer employee
    assigntopeer(id, e) {
        const token = getToken();
        console.log(id);
        
        //Building the Request body of the api

        var newpeer = [{
            PerformanceReviewFor : this.props.match.params.Id,
            PeerReviewdBy : id,
            Period : this.props.match.params.Period

        }]
        const data = {
            data: newpeer
        }
        
        //Used Axios to Assigne Reviews to Peer Employees while passing the token received on Login with the formed request body

        axios.post(`http://localhost:5000/employee_assign`,data, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then(res => {
                console.log(res);          
                alert("Peer Assigned sucessfully");
            })
        .catch(error => {
            alert(error.response.data.message);
              });

    }

    render() {

        return (
            <div>
                <h3>Assign to Peer</h3>

                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Designation</th>
                            <th>Department</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((person) => (
                            <tr>
                                <td>
                                    {person.id}
                                </td>
                                <td>
                                    {person.fname}
                                </td>
                                <td>
                                    {person.email}
                                </td>
                                <td>
                                    {person.department}
                                </td>
                                <td>
                                    {person.designation}
                                </td>

                                <td>
                                 <button className="btn btn-danger" onClick={(e) => this.assigntopeer(person.id, e)}>Assign</button>

                                </td>

                            </tr>
                        )
                        )}

                    </tbody>
                </table>
            </div >
        );
    }
}