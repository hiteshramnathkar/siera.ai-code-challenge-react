import React, { Component } from 'react';
import axios from 'axios';
import { getToken, removeUserSession } from '../src/Utils/Common';


export default class addfeedback extends Component {

    constructor(props) {

        super(props)
        //Used to Bind the fields with Onchange Event

        this.onChangeFeedbackQ1 = this.onChangeFeedbackQ1.bind(this);
        this.onChangeFeedbackQ2 = this.onChangeFeedbackQ2.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        
        //Set Initial State of the fields

        this.state = {
            loading: true,
             feedback_Q1: '',
             feedback_Q2: '',
             data:[]
             }
    }

    //Using ComponentDidMount to fetch data from api & Set State to populate the feedback

    componentDidMount() {
        var token = getToken();
       
        axios.get('http://localhost:5000/performance_review/' + this.props.match.params.Id+'/'+this.props.match.params.Period, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then(res => {
            const data = res.data;
            setTimeout(() => {
                this.setState({ 
                data,
                loading: false
            })}, 500)

        })
        .catch(error => {
            alert(error.response.data.message);
          });

    }

    //handling the on change request of each input field

    onChangeFeedbackQ1(e) {
        this.setState(
            {
                feedback_Q1: e.target.value
            }
        );
    }
    onChangeFeedbackQ2(e) {
        this.setState(
            {
                feedback_Q2: e.target.value
            }
        );
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(`Feedback Q1: ${this.state.feedback_Q1}`);
        console.log(`Feedback Q2: ${this.state.feedback_Q2}`);
       

        //Building the Request body of the api

        var UpdateEmployee =[
            {
                Id: this.state.data[0].Id,
                PeerFeedback: this.state.feedback_Q1
           
            },
            {
                Id: this.state.data[1].Id,
                PeerFeedback: this.state.feedback_Q2
           
            }
        ] 
        const feedback = { data: UpdateEmployee}
        const token = getToken();

        //Used Axios to Add Feedback while passing the token received on Login with the formed request body
        
        axios.post('http://localhost:5000/performance_review/addfeedback', feedback, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then(response => {
            alert("Feedback added sucessfully");

                console.log(JSON.stringify(response));
            })
        .catch(error => {
            alert(error.response.data.message);

            });
        this.props.history.push('/');
    }


    render() {
        const { loading, error, data } = this.state;
        if (loading) {
            return <p>Loading ...</p>;
          }
        
        return (
            <div>
                <h3> Add Feedback about Peer Employee</h3>
                <form onSubmit={this.onSubmit}>
                    
                {this.state.data.map((feedback) => (
                    <div className="form-group">
                    <label>{feedback.PerformanceReviewQuestion}</label> <br/>
                        <select className="form-control" value={feedback.Rating}>
                        <option value="1">Low</option>                     
                        <option value="2">Medium</option>
                        <option value="3">High</option>
                        </select>
                    </div>
                  
                ))}
                
                <div className="form-group">
                        <label>Feedback on Question 1:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.feedback_Q1}
                            onChange={this.onChangeFeedbackQ1} />
                    </div>
                <div className="form-group">
                        <label>Feedback on Question 2:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.feedback_Q2}
                            onChange={this.onChangeFeedbackQ2} />
                    </div>
                    

                    <div className="form-group">
                        <input type="submit" value="Submit" className="btn btn-primary" />
                    </div>

                </form>


            </div>
        );
    }
}
