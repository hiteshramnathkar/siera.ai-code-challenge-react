import React, { Component } from 'react';
import axios from 'axios';
import { useToasts } from 'react-toast-notifications';
import { getToken } from '../src/Utils/Common';
import { Link } from 'react-router-dom';


export default class viewemployee extends Component {

    constructor(props) {

        super(props)

        //Used to Bind the fields with Onchange Event to track modification of the value if any

        this.onChangeFname = this.onChangeFname.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeUserType = this.onChangeUserType.bind(this);
        this.onChangeDesignation = this.onChangeDesignation.bind(this);
        this.onChangeDepartment = this.onChangeDepartment.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
       
       //Set initial state
        this.state = {
            fname:'',
            email:'',
            user_type:'', 
            designation:'',
            department:''
         }



    }

    //Using ComponentDidMount to fetch data from api & Set State to populate the form with employee details

    componentDidMount() {
        var token = getToken();
        
        //Used Axios get request to fetch employee details while passing the token received on Login & employees id

        axios.get('http://localhost:5000/people/' + this.props.match.params.id, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                fname: response.data.fname,
                email: response.data.email,
                user_type: response.data.user_type,
                designation: response.data.designation,
                department: response.data.department
            })
            console.log(response.data.fname);
            console.log(this.state.fname);
        })
        .catch(function(error) {
            alert(error.response.data.message);
            console.log(error);
        });

        
    }

    //handling the on change request of each input field, Can be done with a single function. Couldn't do it now due to current tasks at hand

    onChangeFname(e) {
        this.setState(
            {
                fname: e.target.value
            }
        );
    }
    onChangeEmail(e) {
        this.setState(
            {
                email: e.target.value
            }
        );
    }

    onChangeUserType(e) {
        this.setState(
            {
                user_type: e.target.value
            }
        );
    }
    onChangeDesignation(e) {
        this.setState(
            {
                designation: e.target.value
            }
        );
    }
    onChangeDepartment(e) {
        this.setState(
            {
                department: e.target.value
            }
        );
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(`Name: ${this.state.fname}`);
        console.log(`EMail: ${this.state.email}`);
        console.log(`User type: ${this.state.user_type}`);
        console.log(`Designation: ${this.state.designation}`);
        console.log(`Department: ${this.state.department}`);

        //Building the Request body of the api

        const UpdateEmployee = {
            user_id: this.props.match.params.id,
            fname: this.state.fname,
            email: this.state.email,
            user_type: this.state.user_type,
            designation: this.state.designation,
            department: this.state.department
        }

        //Used Axios put request to update the employee while passing the token received on Login & the updated information

        const token = getToken();
        axios.put('http://localhost:5000/people', UpdateEmployee, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
            .then(response => {
                console.log(JSON.stringify(response));
                alert("Update Successful");

            })
            .catch(error => {
                alert(error.response.data.message);
            });
        
        this.props.history.push('/');


    }


    render() {

        return (
            <div>
                <h3> Edit Employee</h3>

                <form onSubmit={this.onSubmit}>


                    <div className="form-group">
                        <label>Name:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.fname}
                            onChange={this.onChangeFname} />
                    </div>
                    <div className="form-group">
                        <label>Email:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.email}
                            onChange={this.onChangeEmail} />
                    </div>


                    <div className="form-group">
                        <label>User Type:</label> <br />
                        <select className="form-control" value={this.state.user_type} onChange={this.onChangeUserType}>
                            <option value="0">Admin</option>
                            <option value="1">Employee</option>
                        </select>
                    </div>


                    <div className="form-group">
                        <label>Designation:</label> <br />
                        <select className="form-control" value={this.state.designation} onChange={this.onChangeDesignation}>
                            <option value="Executive">Executive</option>
                            <option value="Draftsman">Draftsman</option>
                            <option value="Accountant">Accountant</option>

                        </select>
                    </div>

                    <div className="form-group">
                        <label>Department:</label> <br />
                        <select className="form-control" value={this.state.department} onChange={this.onChangeDepartment}>
                            <option value="Technology">Technology</option>
                            <option value="Accounts">Accounts</option>
                            <option value="Operations">Operations</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <input type="submit" value="Submit" className="btn btn-primary" />
                    </div>

                </form>


            </div>
        );
    }
}
