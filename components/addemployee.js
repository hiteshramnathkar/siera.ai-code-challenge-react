import React, { Component } from 'react';
import axios from 'axios';
import { getToken, removeUserSession } from '../src/Utils/Common';



class addemployee extends Component {

    constructor(props){
       
        super(props);

        //Used to Bind the fields with Onchange Event

        this.onChangeFname=this.onChangeFname.bind(this);
        this.onChangeEmail=this.onChangeEmail.bind(this);
        this.onChangeUserPwd=this.onChangeUserPwd.bind(this);
        this.onChangeUserType=this.onChangeUserType.bind(this);
        this.onChangeDesignation=this.onChangeDesignation.bind(this);
        this.onChangeDepartment=this.onChangeDepartment.bind(this);
        
        this.onSubmit=this.onSubmit.bind(this);
        this.state ={
            fname:'',
            email:'',
            userpwd:'',
            user_type:'1',
            designation:'Executive',
            department:'Technology'

        }
     
    }


    //handling the on change request of each input field

    onChangeFname(e)
    {
        this.setState(
            {
                fname:e.target.value
            }
        );
    }
    onChangeEmail(e)
    {
        this.setState(
            {
                email:e.target.value
            }
        );
    }
    onChangeUserPwd(e)
    {
        this.setState(
            {
                userpwd:e.target.value
            }
        );
    }


    onChangeUserType(e)
    {
        this.setState(
            {
                user_type:e.target.value
            }
        );
    }
    onChangeDesignation(e)
    {
        this.setState(
            {
                designation:e.target.value
            }
        );
    }
    onChangeDepartment(e)
    {
        this.setState(
            {
                department:e.target.value
            }
        );
    }


    onSubmit(e)
    {  
        e.preventDefault();
        console.log(`Name: ${this.state.fname}`);
        console.log(`EMail: ${this.state.email}`);
        console.log(`Password: ${this.state.userpwd}`);
        console.log(`User type: ${this.state.user_type}`);
        console.log(`Designation: ${this.state.designation}`);
        console.log(`Department: ${this.state.department}`);

        //Building the Request body of the api
        
        const NewEmployee = {
            fname:this.state.fname,
            email:this.state.email,
            userpwd:this.state.userpwd,
            user_type:this.state.user_type,
            designation:this.state.designation,
            department:this.state.department
        }

        //Used Axios to Add employees while passing the token received on Login

        const token = getToken();
       
        axios.post('http://localhost:5000/people',NewEmployee,{
            headers: {
                Authorization: "Bearer " + token
            }})
            .then(response => {
                console.log(JSON.stringify(response)); 
                alert("Employee added sucessfully!!");

               // addToast("Employee added sucessfully", { appearance: 'success',autoDismiss: true })
              })
            .catch(error => {
                alert(error.response.data.message);
               // addToast(error.response.data.message, { appearance: 'error',autoDismiss: true })
              });
            
        
    }



    render() {
        //used constant to add toast messages for api

        
        return (
            <div>
                <h3>Add Employee</h3>
               
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.fname}
                            onChange={this.onChangeFname} />
                    </div>
                    <div className="form-group">
                        <label>Email:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.email}
                            onChange={this.onChangeEmail} />
                    </div>
                    <div className="form-group">
                        <label>Password:</label>
                        <input type="password"
                            className="form-control"
                            value={this.state.userpwd}
                            onChange={this.onChangeUserPwd} />
                    </div>

                    <div className="form-group">
                        <label>User Type:</label> <br/>
                        <select className="form-control" value={this.state.user_type} onChange={this.onChangeUserType}>
                        <option value="0">Admin</option>                     
                        <option value="1">Employee</option>
                        </select>
                    </div>


                    <div className="form-group">
                        <label>Designation:</label> <br/>
                        <select className="form-control" value={this.state.designation} onChange={this.onChangeDesignation}>
                            <option value="Executive">Executive</option>
                            <option value="Draftsman">Draftsman</option>
                            <option value="Accountant">Accountant</option>
                          
                        </select>
                    </div>
                    
                    <div className="form-group">
                        <label>Department:</label> <br/>
                        <select className="form-control" value={this.state.department} onChange={this.onChangeDepartment}>
                            <option value="Technology">Technology</option>
                            <option value="Accounts">Accounts</option>
                            <option value="Operations">Operations</option>
                        </select>
                    </div>                   
                    <div className="form-group">
                        <input type="submit" value="Submit" className="btn btn-primary" />
                    </div>
                  
                </form>
            </div>
        );
    }
}
export default addemployee;