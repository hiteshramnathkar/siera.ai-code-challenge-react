import React, { Component } from 'react';
import axios from 'axios';
import { useToasts } from 'react-toast-notifications';
import { getToken } from '../src/Utils/Common';
import { Link } from 'react-router-dom';

export default class listreviews extends Component {
    state = { data: [] }

    //Using ComponentDidMount to fetch data from api & Set State to populate the list
    componentDidMount() {
        const token = getToken();

        //Used Axios get request to get performance reviews while passing the token received on Login

        axios.get('http://localhost:5000/performance_review', {
            headers: {
                Authorization: "Bearer " + token
            }
        })
            .then(res => {
                const data = res.data;
                this.setState({ data });
            })
            .catch(error => {
                alert(error.response.data.message);
                
            });


    }

    render() {
        return (
            <div>
                <h3> List Reviews</h3>

                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Full Name</th>
                            <th>Period</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((person) => (
                            <tr>
                                <td>
                                    {person.Id}
                                </td>
                                <td>
                                    {person.fname}
                                </td>
                                <td>
                                    {person.Period}
                                </td>


                                <td>
                                    <Link to={"/assigntopeer/" + person.Id + "/" + person.Period}>Assign to Peer</Link>

                                </td>

                            </tr>
                        )
                        )}

                    </tbody>
                </table>
            </div >
        );
    }
}


