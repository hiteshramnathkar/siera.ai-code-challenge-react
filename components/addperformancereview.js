import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {  useToasts } from 'react-toast-notifications';
import { getToken, removeUserSession } from '../src/Utils/Common';



export default class addperformancereview extends Component {

    
    constructor(props){
       
        super(props);
        
        //Used to Bind the fields with Onchange Event

        this.onChangeQ1Rating=this.onChangeQ1Rating.bind(this);
        this.onChangeQ2Rating=this.onChangeQ2Rating.bind(this);
        this.onChangePeriod=this.onChangePeriod.bind(this);

        this.onSubmit=this.onSubmit.bind(this);

        //Set Initial State of the fields

        this.state ={
            Q1:'',
            Q2:'',
            Parameter1:'Sales Targets Completed',
            Parameter2:'Assertive Nature',
            Period:''
        }
     
    }

    //handling the on change request of each input field

    onChangeQ1Rating(e)
    {
        this.setState(
            {
                Q1:e.target.value
            }
        );
    }
    onChangeQ2Rating(e)
    {
        this.setState(
            {
                Q2:e.target.value
            }
        );
    }
    onChangePeriod(e)
    {
        this.setState(
            {
                Period:e.target.value
            }
        );
    }
  

    onSubmit(e)
    {
        e.preventDefault();
        console.log(`Q1: ${this.state.Q1}`);
        console.log(`Q2: ${this.state.Q2}`);
        
        //Building the Request body of the api

        var newreview = [{
            PerformanceReviewQuestion:this.state.Parameter1,
            Rating:this.state.Q1,
            PerformanceReviewFor:this.props.match.params.id,
            Period:this.state.Period,
            QuestionId:1,
        },
        {
            PerformanceReviewQuestion:this.state.Parameter2,
            Rating:this.state.Q2,
            PerformanceReviewFor:this.props.match.params.id,
            Period:this.state.Period,
            QuestionId:2,
        }]

        const data = {
            data: newreview
        }

        const token = getToken();
        //Used Axios to Add Reviews while passing the token received on Login with the formed request body

        axios.post('http://localhost:5000/performance_review',data,{
            headers: {
                Authorization: "Bearer " + token
            }})
            .then(response => {
                console.log(JSON.stringify(response));
                alert("Perfomance Review added sucessfully");
              })
            .catch(error => {
                alert(error.response.data.message);
              });
        
    }

    
  
    render(){
        return (
            <div>
                <h3>Add Performance Review</h3>
               
               <form onSubmit={this.onSubmit}>
                   
               <div className="form-group">
                        <label>Period:</label>
                        <input type="text"
                            className="form-control"
                            value={this.state.Period}
                            onChange={this.onChangePeriod} />
                    </div>

                   <div className="form-group">
                       <label>{this.state.Parameter1}</label>
                       <label>Rating:</label> <br/>
                       <select className="form-control" value={this.state.Q1} onChange={this.onChangeQ1Rating}>
                       <option value="1">Low</option>                     
                       <option value="2">Medium</option>
                       <option value="3">High</option>

                       </select>
                   </div>


                   <div className="form-group">
                      <label>{this.state.Parameter2}</label>
                       <label>Rating:</label> <br/>
                       <select className="form-control" value={this.state.Q2} onChange={this.onChangeQ2Rating}>
                           <option value="1">Low</option>
                           <option value="2">Medium</option>
                           <option value="3">High</option>
                         
                       </select>
                   </div>
                   <div className="form-group">
                       <input type="submit" value="Submit" className="btn btn-primary" />
                   </div>

               </form>
          
            </div>
        );
    }
}