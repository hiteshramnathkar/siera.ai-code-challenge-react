import React, { Component } from 'react';
import axios from 'axios';
import { getToken,getUserid } from '../src/Utils/Common';

import { Link } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';


export default class listreviewspeer extends Component {
    state = { data: [] }
    
    //Using ComponentDidMount to fetch data from api & Set State to populate the list

    componentDidMount() {
        const token = getToken();
        var userid = getUserid();

        //Used Axios get request to get list reviews assigned to the employee while passing the token received on Login & employees id

        axios.get('http://localhost:5000/employee_assign/'+userid, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
            .then(res => {
                const data = res.data;
                this.setState({ data });

            })
            .catch(error => {
                alert(error.response.data.message);
            });
    }
    
    render() {

        return (
            <div>
                <h3> List Reviews</h3>

                <table className="table">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Full Name</th>
                            <th>Period</th>
                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((person) => (
                            <tr>
                                <td>
                                    {person.Id}
                                </td>
                                <td>
                                    {person.fname}
                                </td>
                                <td>
                                    {person.Period}
                                </td>
                              

                                <td>
                                <Link to={"/addfeedback/"+person.Id+"/"+person.Period}>Add Feedback</Link> 

                                </td>

                            </tr>
                        )
                        )}

                    </tbody>
                </table>
            </div >
        );
    }
}


